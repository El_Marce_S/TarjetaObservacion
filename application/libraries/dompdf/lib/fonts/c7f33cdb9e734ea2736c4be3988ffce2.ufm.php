<?php return array (
  'codeToName' => 
  array (
  ),
  'isUnicode' => true,
  'EncodingScheme' => 'FontSpecific',
  'FontName' => 'Roboto Thin',
  'FullName' => 'Roboto Thin',
  'Version' => 'Version 2.137; 2017',
  'PostScriptName' => 'Roboto-Thin',
  'Weight' => 'Medium',
  'ItalicAngle' => '0',
  'IsFixedPitch' => 'false',
  'UnderlineThickness' => '49',
  'UnderlinePosition' => '-73',
  'FontHeightOffset' => '0',
  'Ascender' => '928',
  'Descender' => '-244',
  'FontBBox' => 
  array (
    0 => '-732',
    1 => '-271',
    2 => '1127',
    3 => '1056',
  ),
  'StartCharMetrics' => '428',
  'C' => 
  array (
    0 => 0.0,
    13 => 239.0,
    32 => 239.0,
    33 => 194.0,
    34 => 254.0,
    35 => 547.0,
    36 => 547.0,
    37 => 746.0,
    38 => 609.0,
    39 => 166.0,
    40 => 296.0,
    41 => 304.0,
    42 => 418.0,
    43 => 562.0,
    44 => 187.0,
    45 => 296.0,
    46 => 214.0,
    47 => 382.0,
    48 => 547.0,
    49 => 547.0,
    50 => 547.0,
    51 => 547.0,
    52 => 547.0,
    53 => 547.0,
    54 => 547.0,
    55 => 547.0,
    56 => 547.0,
    57 => 547.0,
    58 => 178.0,
    59 => 178.0,
    60 => 514.0,
    61 => 558.0,
    62 => 514.0,
    63 => 436.0,
    64 => 929.0,
    65 => 597.0,
    66 => 603.0,
    67 => 648.0,
    68 => 654.0,
    69 => 569.0,
    70 => 572.0,
    71 => 687.0,
    72 => 702.0,
    73 => 261.0,
    74 => 549.0,
    75 => 635.0,
    76 => 516.0,
    77 => 857.0,
    78 => 708.0,
    79 => 666.0,
    80 => 601.0,
    81 => 666.0,
    82 => 654.0,
    83 => 591.0,
    84 => 598.0,
    85 => 667.0,
    86 => 597.0,
    87 => 906.0,
    88 => 597.0,
    89 => 597.0,
    90 => 598.0,
    91 => 214.0,
    92 => 378.0,
    93 => 214.0,
    94 => 415.0,
    95 => 413.0,
    96 => 262.0,
    97 => 528.0,
    98 => 547.0,
    99 => 507.0,
    100 => 547.0,
    101 => 504.0,
    102 => 315.0,
    103 => 548.0,
    104 => 547.0,
    105 => 206.0,
    106 => 217.0,
    107 => 473.0,
    108 => 206.0,
    109 => 896.0,
    110 => 547.0,
    111 => 550.0,
    112 => 547.0,
    113 => 547.0,
    114 => 334.0,
    115 => 498.0,
    116 => 316.0,
    117 => 547.0,
    118 => 478.0,
    119 => 757.0,
    120 => 478.0,
    121 => 478.0,
    122 => 478.0,
    123 => 322.0,
    124 => 198.0,
    125 => 322.0,
    126 => 689.0,
    160 => 239.0,
    161 => 198.0,
    162 => 542.0,
    163 => 562.0,
    164 => 733.0,
    165 => 507.0,
    166 => 194.0,
    167 => 597.0,
    168 => 442.0,
    169 => 813.0,
    170 => 438.0,
    171 => 442.0,
    172 => 538.0,
    173 => 296.0,
    174 => 817.0,
    175 => 396.0,
    176 => 382.0,
    177 => 526.0,
    178 => 355.0,
    179 => 355.0,
    180 => 250.0,
    181 => 547.0,
    182 => 462.0,
    183 => 230.0,
    184 => 239.0,
    185 => 355.0,
    186 => 446.0,
    187 => 438.0,
    188 => 716.0,
    189 => 736.0,
    190 => 774.0,
    191 => 445.0,
    192 => 597.0,
    193 => 597.0,
    194 => 597.0,
    195 => 597.0,
    196 => 597.0,
    197 => 597.0,
    198 => 887.0,
    199 => 648.0,
    200 => 569.0,
    201 => 569.0,
    202 => 569.0,
    203 => 569.0,
    204 => 261.0,
    205 => 261.0,
    206 => 261.0,
    207 => 261.0,
    208 => 669.0,
    209 => 708.0,
    210 => 666.0,
    211 => 666.0,
    212 => 666.0,
    213 => 666.0,
    214 => 666.0,
    215 => 514.0,
    216 => 666.0,
    217 => 667.0,
    218 => 667.0,
    219 => 667.0,
    220 => 667.0,
    221 => 597.0,
    222 => 595.0,
    223 => 578.0,
    224 => 528.0,
    225 => 528.0,
    226 => 528.0,
    227 => 528.0,
    228 => 528.0,
    229 => 528.0,
    230 => 847.0,
    231 => 507.0,
    232 => 504.0,
    233 => 504.0,
    234 => 504.0,
    235 => 504.0,
    236 => 198.0,
    237 => 198.0,
    238 => 198.0,
    239 => 198.0,
    240 => 578.0,
    241 => 547.0,
    242 => 550.0,
    243 => 550.0,
    244 => 550.0,
    245 => 550.0,
    246 => 550.0,
    247 => 566.0,
    248 => 547.0,
    249 => 547.0,
    250 => 547.0,
    251 => 547.0,
    252 => 547.0,
    253 => 478.0,
    254 => 547.0,
    255 => 478.0,
    256 => 597.0,
    257 => 528.0,
    258 => 597.0,
    259 => 528.0,
    260 => 597.0,
    261 => 528.0,
    262 => 648.0,
    263 => 507.0,
    264 => 648.0,
    265 => 507.0,
    266 => 648.0,
    267 => 507.0,
    268 => 648.0,
    269 => 507.0,
    270 => 654.0,
    271 => 621.0,
    272 => 669.0,
    273 => 547.0,
    274 => 569.0,
    275 => 504.0,
    276 => 569.0,
    277 => 504.0,
    278 => 569.0,
    279 => 504.0,
    280 => 569.0,
    281 => 504.0,
    282 => 569.0,
    283 => 504.0,
    284 => 687.0,
    285 => 548.0,
    286 => 687.0,
    287 => 548.0,
    288 => 687.0,
    289 => 548.0,
    290 => 687.0,
    291 => 548.0,
    292 => 702.0,
    293 => 547.0,
    294 => 678.0,
    295 => 562.0,
    296 => 261.0,
    297 => 198.0,
    298 => 261.0,
    299 => 198.0,
    300 => 261.0,
    301 => 198.0,
    302 => 261.0,
    303 => 206.0,
    304 => 261.0,
    305 => 198.0,
    306 => 810.0,
    307 => 423.0,
    308 => 549.0,
    309 => 206.0,
    310 => 635.0,
    311 => 473.0,
    312 => 529.0,
    313 => 516.0,
    314 => 206.0,
    315 => 516.0,
    316 => 206.0,
    317 => 516.0,
    318 => 279.0,
    319 => 516.0,
    320 => 313.0,
    321 => 518.0,
    322 => 247.0,
    323 => 708.0,
    324 => 547.0,
    325 => 708.0,
    326 => 547.0,
    327 => 708.0,
    328 => 547.0,
    329 => 547.0,
    330 => 708.0,
    331 => 547.0,
    332 => 666.0,
    333 => 550.0,
    334 => 666.0,
    335 => 550.0,
    336 => 666.0,
    337 => 550.0,
    338 => 915.0,
    339 => 939.0,
    340 => 654.0,
    341 => 334.0,
    342 => 654.0,
    343 => 334.0,
    344 => 654.0,
    345 => 334.0,
    346 => 591.0,
    347 => 498.0,
    348 => 591.0,
    349 => 498.0,
    350 => 591.0,
    351 => 498.0,
    352 => 591.0,
    353 => 498.0,
    354 => 598.0,
    355 => 316.0,
    356 => 598.0,
    357 => 336.0,
    358 => 598.0,
    359 => 316.0,
    360 => 667.0,
    361 => 547.0,
    362 => 667.0,
    363 => 547.0,
    364 => 667.0,
    365 => 547.0,
    366 => 667.0,
    367 => 547.0,
    368 => 667.0,
    369 => 547.0,
    370 => 667.0,
    371 => 547.0,
    372 => 906.0,
    373 => 757.0,
    374 => 597.0,
    375 => 478.0,
    376 => 597.0,
    377 => 598.0,
    378 => 478.0,
    379 => 598.0,
    380 => 478.0,
    381 => 598.0,
    382 => 478.0,
    383 => 206.0,
    399 => 709.0,
    402 => 314.0,
    416 => 708.0,
    417 => 548.0,
    431 => 667.0,
    432 => 553.0,
    496 => 206.0,
    506 => 597.0,
    507 => 528.0,
    508 => 887.0,
    509 => 847.0,
    510 => 666.0,
    511 => 547.0,
    536 => 591.0,
    537 => 498.0,
    538 => 598.0,
    539 => 316.0,
    567 => 206.0,
    710 => 389.0,
    730 => 286.0,
    732 => 399.0,
    7680 => 597.0,
    7681 => 528.0,
    7742 => 857.0,
    7743 => 896.0,
    7808 => 906.0,
    7809 => 757.0,
    7810 => 906.0,
    7811 => 757.0,
    7812 => 906.0,
    7813 => 757.0,
    7922 => 597.0,
    7923 => 478.0,
    7924 => 597.0,
    7925 => 478.0,
    7926 => 597.0,
    7927 => 478.0,
    7928 => 597.0,
    7929 => 478.0,
    8211 => 646.0,
    8212 => 762.0,
    8216 => 156.0,
    8217 => 156.0,
    8218 => 146.0,
    8220 => 244.0,
    8221 => 245.0,
    8222 => 235.0,
    8226 => 310.0,
    8230 => 613.0,
    8249 => 295.0,
    8250 => 295.0,
    8260 => 442.0,
    8308 => 355.0,
    8355 => 572.0,
    8356 => 562.0,
    8358 => 747.0,
    8359 => 815.0,
    8360 => 1035.0,
    8361 => 779.0,
    8362 => 754.0,
    8363 => 562.0,
    8364 => 547.0,
    8369 => 641.0,
    8377 => 460.0,
    8378 => 470.0,
    8380 => 653.0,
    8381 => 620.0,
    8722 => 570.0,
  ),
  'CIDtoGID_Compressed' => true,
  'CIDtoGID' => 'eJzt2/XTWMUZBeBzcA8ePMHdgntwgrt7ICR4cHe3BAjupdBCCy1UkRb5O6CFClSQFmmhhZYMk+l0OhACzTffMDzPzN33zu7e3XPn3l83zeeZ8XNH/ttMmTmzZNbMltkzR+bMXJk782RI5s18mT8LZMEslIUzNItk0SyWxbNElsxSGZbhWTrLZNksl+WzQlbMSlk5q2TVrJbVs0bWzFpZOyOyTtbNelk/G2TDbJSNs0k2zWbZPFtkZLbMVtk622TbbJfts0NGZcfslJ2zS3bNbtk9e2TP7JW9s0/2zX7ZPwfkwByUg3NIDs1hOTxH5MhpesOpeymjc1SOzpgck7EZl2NzXI7PCTkxL+eknJzxOSWn5rScnjNyZs7K2Tkn5+a8nJ8LcmEuym/zu/w+r+X1/CEX50/5c97Im3krb+cv+Wveya/zbt7L+/lb/p4PckkuzYf5R/6Zj/JxLsvl+Vf+3bSdoTPmis7cWTprZ+vsnaNzdq7OnSs7T4d03s7X+btArsrVXbALdeEO7SK5pot2sS7eJbpkl+qwDu/SXabLdrku3xW6YlfqyvlVru0qXbWrdfWu0TW7VtfuiK7Tdbte1+8G3bAbdeNu0k27Wa7LK928W3Rkt+xW3brbdNtul+u7fXfoqO7Ynbpzbugu3bW7dffu0T27V/fOhEzsPt23+3X/HtADe1BuzE09uIf00B7Ww3tEbs6kHtnRPapHd0yP6diO67E9rsf3hJ7c8T2tp/eMnplX85ue1bN7Ts/teT2/F/TCXtSLe0kv7WW9vFf0yl7Vq3tNr+11vb435JYv+Lq3ftreNpUZt+eOqYzembu+7A81ffWz9v9jZ+qETuyNvelLr3diT+opPfUrxrn7Kz73zXDPNM57YPL14EAGAQAAAAAAAAAAAAAAAGCw9d7eN2h7391pPfE2cBlu7qTe0lt7W28f7CwDpXf0zt7fB/qtPthv96HBzsPXznfzyJS7R/O9fH9yfSyP5wdT+n44pT4xTWs9mR99Ru+P/698X+wnA7l4v5OfTi4/y8/zVJ7OM304z/5n8Bf/M/mXeW5y+3xeGMhEMN28ONgBAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAIBvkk8ACVSdpw==',
  '_version_' => 6,
);