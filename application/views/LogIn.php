<!DOCTYPE html>
<html lang="en" >

<head>
    <meta charset="UTF-8">
    <title>Ingreso a Sistema Integral</title>

    <link rel="stylesheet" href="https://code.getmdl.io/1.3.0/material.teal-yellow.min.css"/>
    <script defer src="https://code.getmdl.io/1.3.0/material.min.js"></script>
    <meta charset="utf-8">

    <link rel='stylesheet prefetch' href='http://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css'>
    <link rel='stylesheet prefetch' href='http://fonts.googleapis.com/css?family=Roboto:400,100,300,500,700,900&subset=latin,latin-ext'>

    <link rel="stylesheet" href="<?php echo base_url(); ?>Assets/LogIn_Template/style.css">

</head>

<body>

<div class="materialContainer">



    <form action="<?php echo base_url()?>index.php/auth/LogIn" method="post">
        <div class="box">


            <?php if($this->session->flashdata("error")):?>

                <div class="alert alert-danger">

                <p> <?php ($this->session->flashdata('error'))  ?> </p>
                </div>

            <?php endif ; ?>


            <div class="title">Ingreso al Sistema</div>
            <div class="input">
                <label for="username">Nombre de Usuario</label>
                <input type="text"  name="username" id="username">
            <span class="spin"></span>
        </div>

        <div class="input">
            <label for="password">Contraseña</label>
            <input type="password" name="password" id="password">
            <span class="spin"></span>
        </div>

        <br>

            <div class="button login" >
                <button type="submit"><span>Ingresar</span> <i class="fa fa-check"></i></button>
            </div>
    </div>

    </form>




</div>
<script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>



<script  src="<?php echo base_url(); ?>Assets/LogIn_Template/index.js"></script>




</body>

</html>




