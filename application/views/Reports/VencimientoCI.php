<!DOCTYPE html>

<html>
<head>


    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Reporte de VencimientosE</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="../../bower_components/bootstrap/dist/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="../../bower_components/font-awesome/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="../../bower_components/Ionicons/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="../../dist/css/AdminLTE.min.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- Google Font -->
    <link rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>


<body onload="window.print();">
<div class="wrapper">
    <!-- Main content -->
    <section class="invoice">
        <!-- title row -->
        <div class="row">
            <div class="col-xs-12">
                <=" class="page-header">
                <i class="fa fa-globe"></i>
                <p><img src="../Empro/Assets/img/empro_logo.png" align="middle"></p>
                <h1>Reporte de Cedulas Vencidas a la fecha. </h1>
                </h2>
            </div>
            <!-- /.col -->


        </div>
        <!-- info row -->
        <div class="row invoice-info">
            <div class="col-sm-4 invoice-col">
                <address>
                    <h2>Documento Generado en: <?php
                        echo date("Y/m/d") . "<br>"; ?>
                        <strong></strong><br></h2>
                </address>
            </div>
        </div>

        <!-- Table row -->
        <div class="row">
            <div class="col-xs-12 table-responsive">
                <table class="table table-striped">
                    <tr>
                        <th>Codigo Empro</th>
                        <th>Nombre Completo</th>
                        <th>Carnet de Identidad</th>
                        <th>Expirado</th>
                    </tr>
                    <tbody>
                    <?php
                    foreach ($result as $row) {
                        ?>
                        <tr>
                            <td><?php echo $row->x_employee_ ?></td>
                            <td><?php echo $row->x_legal_nam ?></td>
                            <td><?php echo $row->identificat ?></td>
                            <td><?php echo $row->x_expiracio ?></td>
                        </tr>

                    <?php }
                    ?>
                    </tbody>
                </table>
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->

