<div class="right-invoice">
    <div class="row invoice-info">
        <div class="col-sm-4 invoice-col">
            <address>
                <h2><strong>Reporte de Cedulas de Identidad sin fecha de expiración registrada: </strong><br></h2>
            </address>
        </div>
    </div>
</div>

<section id="items">

    <table cellpadding="0" cellspacing="0">

        <thead>
        <tr>
            <th>Codigo Empro</th>
            <th>Nombre Completo</th>
            <th>Carnet de Identidad</th>
            <th>Expirado</th>
        </tr>
        </thead>

        <tbody>
        <?php
        foreach ($result as $row) {
            ?>
            <tr>
                <td><?php echo $row->x_employee_ ?></td>
                <td><?php echo $row->x_legal_nam ?></td>
                <td><?php echo $row->identificat ?></td>
                <td><?php echo $row->x_expiracio ?></td>
            </tr>

        <?php }
        ?>

        </tbody>
        </tr>
    </table>
</section>
</div>
</div>
</div>
<script src="http://cdn.invoicebus.com/generator/generator.min.js?data=data.js"></script>
</body>
</html>
