<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Panel de Control </title>


    <!-- Bootstrap -->
    <link href="<?php echo base_url() ?>Assets/Gentelella/vendors/bootstrap/dist/css/bootstrap.min.css"
          rel="stylesheet">
    <!-- Font Awesome -->
    <link href="<?php echo base_url() ?>Assets/Gentelella/vendors/font-awesome/css/font-awesome.min.css"
          rel="stylesheet">
    <!-- NProgress -->
    <link href="<?php echo base_url() ?>Assets/Gentelella/vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- Custom Theme Style -->
    <link href="<?php echo base_url() ?>Assets/Gentelella/build/css/custom.min.css" rel="stylesheet">
    <!-- bootstrap-datetimepicker -->
    <link href="<?php echo base_url() ?>Assets/Gentelella/vendors/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.css"
          rel="stylesheet">
</head>

<body class="nav-md">
<div class="container body">
    <div class="main_container">
        <div class="col-md-3 left_col">
            <div class="left_col scroll-view">
                <div class="navbar nav_title" style="border: 0;">
                    <a href="<?php echo base_url() ?>index.php/SAdmin/SAdmin/Home" class="site_title"><i
                                class="fa fa-home"></i> <span>Inicio</span></a>
                </div>

                <div class="clearfix"></div>

                <!-- menu profile quick info -->
                <div class="profile clearfix">
                    <div class="profile_pic">
                        <img src="<?php echo base_url() ?>Assets/ProfilePics/<?php echo $this->session->userdata('Foto'); ?>"
                             alt="..." class="img-circle profile_img">

                    </div>
                    <div class="profile_info">
                        <span>Bienvenid@,</span>
                        <h2><?php echo $this->session->userdata('Nombre'); ?></h2>
                    </div>
                </div>
                <!-- /menu profile quick info -->

                <br/>