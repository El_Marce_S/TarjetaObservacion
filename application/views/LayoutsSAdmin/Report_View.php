<!-- page content -->
<div class="right_col" role="main">

    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h3>Vencimiento de Documentos </h3>
            </div>
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Lista de Vencimientos de Cédula de Identidad a La Fecha<br>
                            <small>Cedulas Vencidas<p id="date"></p></small>
                        </h2>
                        <p style="text-align: right">
                            <a href="<?php echo base_url()?>index.php/Printables/VencimientosReport/Reporte">
                                <button class="btn btn-success">Ver PDF</button>
                            </a>
                        </p>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">

                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th>Codigo Empro</th>
                                <th>Nombre Completo</th>
                                <th>Carnet de Identidad</th>
                                <th>Expirado</th>
                            </tr>
                            </thead>

                            <tbody>
                            <?php

                            foreach($result  as $row) {
                                ?>
                                <tr>
                                    <td><?php echo $row->x_employee_ ?></td>
                                    <td><?php echo $row->x_legal_nam ?></td>
                                    <td><?php echo $row->identificat ?></td>
                                    <td><?php echo $row->x_expiracio ?></td>
                                </tr>

                            <?php }
                            ?>
                            </tbody>
                        </table>
                    </div>
                </div>