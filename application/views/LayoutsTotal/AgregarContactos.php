<!-- page content -->
<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h3>Registro de nuevos datos de contáctos</h3>



            </div>

            <div class="clearfix"></div>
            <div class="col-md-3 col-sm-3 col-xs-12"></div>
            <?php if (isset($_SESSION['DONE'])) { ?>
                <div class="alert alert-success col-md-6 col-sm-6 col-xs-12 "><h2><?php echo $_SESSION ['DONE']; ?><br>
                    </h2></div>
                <?php
            }
            ?>

            <div class="x_content">
            </div>

            <br/>
            <form action="<?php echo base_url() ?>index.php/Recepcion/RecepController/Agregar_Contactos" method="post"
                  id="demo-form2" data-parsley-validate class="form-horizontal form-label-left">

                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Nombre Completo y
                        Apellidos</label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <input type="text" id="first-name" class="form-control col-md-7 col-xs-12" name="NOMBRE">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Teléfono de contacto
                        1</label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <input type="text" id="last-name" name="Tel1"
                               class="form-control col-md-7 col-xs-12">
                    </div>
                </div>
                <div class="form-group">
                    <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Teléfono de contacto
                        2</label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="middle-name" class="form-control col-md-7 col-xs-12" type="text" name="Tel2">
                    </div>
                </div>
                <div class="form-group">
                    <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Telefono IP</label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="middle-name" class="form-control col-md-7 col-xs-12" type="text" name="TelIP">
                    </div>
                </div>
                <div class="form-group">
                    <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Celular de
                        contacto</label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="middle-name" class="form-control col-md-7 col-xs-12" type="text" name="Cel">
                    </div>
                </div>
                <div class="form-group">
                    <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Especialidad o
                        Referencia</label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <input id="middle-name" class="form-control col-md-7 col-xs-12" type="text" name="Ref">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Select</label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <select class="form-control" data-dpmaxz-eid="31" name="Obs">
                            <option value="_">Elija una Opción</option>
                            <option value="POL20">Polliconsultorio 20 de Octubre</option>
                            <option value="ARCE">Policonsultorio Central (Arce)</option>
                            <option value="POLEA">Policonsultorio El Alto</option>
                            <option value="HPO">Hospital Petrolero de Obrajes</option>
                            <option value="OBI">Obispo Cardenas</option>
                            <option value="OFNAL">Oficina Nacional</option>
                            <option value="PART">Particular</option>
                        </select>
                    </div>
                </div>


                <div class="form-group">
                    <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                        <button class="btn btn-primary" type="button">Cancelar</button>
                        <button class="btn btn-primary" type="reset">Limpiar Registros</button>
                        <button type="submit" name="Guardar" value="Guardar"
                                class="btn btn-success">Guardar registros
                        </button>


                    </div>
                </div>

            </form>
        </div>
    </div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>

<!-- /page content -->

<!-- footer content -->
<footer>
    <div class="pull-right">
        Gentelella - Bootstrap Admin Template by <a href="https://colorlib.com">Colorlib</a>
    </div>
    <div class="clearfix"></div>
</footer>
<!-- /footer content -->
</div>
</div>

<!-- jQuery -->
<script src="<?php echo base_url(); ?>Assets/Gentelella/vendors/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap -->
<script src="<?php echo base_url(); ?>Assets/Gentelella/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- FastClick -->
<script src="<?php echo base_url(); ?>Assets/Gentelella/vendors/fastclick/lib/fastclick.js"></script>
<!-- NProgress -->
<script src="<?php echo base_url(); ?>Assets/Gentelella/vendors/nprogress/nprogress.js"></script>
<!-- jQuery Smart Wizard -->
<script src="<?php echo base_url(); ?>Assets/Gentelella/vendors/jQuery-Smart-Wizard/js/jquery.smartWizard.js"></script>
<!-- Custom Theme Scripts -->
<script src="<?php echo base_url(); ?>Assets/Gentelella/build/js/custom.min.js"></script>


<!-- bootstrap-daterangepicker -->
<script src="<?php echo base_url(); ?>Assets/Gentelella/vendors/moment/min/moment.min.js"></script>
<script src="<?php echo base_url(); ?>Assets/Gentelella/vendors/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- bootstrap-datetimepicker -->
<script src="<?php echo base_url(); ?>Assets/Gentelella/vendors/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>
<!-- Initialize datetimepicker -->

<script>
    $('#myDatepicker').datetimepicker({
        format: 'YYYY-MM-DD'
    });

    $('#myDatepicker2').datetimepicker({
        format: 'YYYY-MM-DD'
    });

    $('#myDatepicker3').datetimepicker({
        format: 'YYYY-MM-DD'
    });

</script>


</body>
</html>