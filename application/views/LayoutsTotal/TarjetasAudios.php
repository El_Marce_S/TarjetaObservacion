<!-- page content -->
<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h3>Audios</h3>
            </div>
            <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                </div>
            </div>
        </div>
    </div>

    <div class="clearfix"></div>

    <div class="col-md-12 col-sm-12 col-xs-12">

        <div class="x_content" onload="busc">
            <p class="text-muted font-13 m-b-30">
                Lista de Audios con Transcripción Pendiente
            </p>
            <button type="button" id="BuscarAudios" class="btn btn-flat"><i class="fa fa-search"></i> &nbsp;Buscar
            </button>
            <table id="tabla_audios" class="table table-striped table-bordered dt-responsive nowrap"
                   cellspacing="0" width="100%">
                <thead>
                <tr>
                    <th>Topico</th>
                    <th>Nombre de Archivo de Audio</th>
                    <th>Lugar</th>
                    <th>Fecha de Reporte</th>
                    <th>Hora de Reporte</th>
                    <th>Regla de Oro</th>
                    <th>Fecha de Subida</th>
                    <th>Transcripción</th>
                </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>
</div>
</div>
<!-- Modal Cierre -->
<div class="modal fade" id="ModalAgregarTranscripcion" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header ">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Agregar Transcripcón</h4>
            </div>

            <div class="modal-body" >
                <form class="form-horizontal"
                      action="<?php echo base_url() ?>index.php/Total/TotalController/TarjetasNuevas" method="post">
                    <!-- parametros ocultos -->
                    <input type="hidden" id="modal_IdTarjeta" name="modal_IdTarjeta">
                    <div class="box-body">
						<div class="form-group">
							<label class="col-sm-3 control-label">Audio</label>
							<div class="col-sm-9">
									<audio id="modal_AudioDescripcion_src"  type="audio/mp3" />
							</div>
						</div>

                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label">Transcripcion</label>
                            <div class="col-sm-9">
                                <textarea rows="4" cols="50" type="text" name="modal_Transcripcion" class="form-control"
										  id="modal_Transcripcion" value=""> </textarea>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" id="mbtnCerrarModal" data-dismiss="modal">
                            Cancelar
                        </button>
                        <button type="submit" class="btn btn-info" id="mbtnUpdPerona" name="Actualizar"
                                value="Actualizar">Actualizar
                        </button>
                </form>
            </div>
        </div>
    </div>
</div>
</div>
</div>
</div>
</div>
</div>
<!-- /page content -->

<!-- footer content -->
<footer>
    <div class="pull-right">
        Desarrollado Por <a href="http://www.emproltda.com">Empro LTDA</a>
    </div>
    <div class="clearfix"></div>
</footer>
<!-- /footer content -->
</div>
</div>

<!-- jQuery -->
<script src="<?php echo base_url(); ?>Assets/Gentelella/vendors/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap -->
<script src="<?php echo base_url(); ?>Assets/Gentelella/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- FastClick -->
<script src="<?php echo base_url(); ?>Assets/Gentelella/vendors/fastclick/lib/fastclick.js"></script>
<!-- NProgress -->
<script src="<?php echo base_url(); ?>Assets/Gentelella/vendors/nprogress/nprogress.js"></script>
<!-- jQuery Smart Wizard -->
<script src="<?php echo base_url(); ?>Assets/Gentelella/vendors/jQuery-Smart-Wizard/js/jquery.smartWizard.js"></script>
<!-- Custom Theme Scripts -->
<script src="<?php echo base_url(); ?>Assets/Gentelella/build/js/custom.min.js"></script>


<!-- iCheck -->
<script src="<?php echo base_url(); ?>Assets/Gentelella/vendors/iCheck/icheck.min.js"></script>
<!-- Datatables -->
<script src="<?php echo base_url(); ?>Assets/Gentelella/vendors/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url(); ?>Assets/Gentelella/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<script src="<?php echo base_url(); ?>Assets/Gentelella/vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
<script src="<?php echo base_url(); ?>Assets/Gentelella/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
<script src="<?php echo base_url(); ?>Assets/Gentelella/vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
<script src="<?php echo base_url(); ?>Assets/Gentelella/vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
<script src="<?php echo base_url(); ?>Assets/Gentelella/vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
<script src="<?php echo base_url(); ?>Assets/Gentelella/vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
<script src="<?php echo base_url(); ?>Assets/Gentelella/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
<script src="<?php echo base_url(); ?>Assets/Gentelella/vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
<script src="<?php echo base_url(); ?>Assets/Gentelella/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
<script src="<?php echo base_url(); ?>Assets/Gentelella/vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
<script src="<?php echo base_url(); ?>Assets/Gentelella/vendors/jszip/dist/jszip.min.js"></script>
<script src="<?php echo base_url(); ?>Assets/Gentelella/vendors/pdfmake/build/pdfmake.min.js"></script>
<script src="<?php echo base_url(); ?>Assets/Gentelella/vendors/pdfmake/build/vfs_fonts.js"></script>
<script src="<?php echo base_url(); ?>Assets/JS/AudioTranscript.js"></script>
<script src="<?php echo base_url(); ?>Assets/JS/AudioPlayer/audio.min.js"></script>

</body>
</html>

<script>
    var baseurl = "<?php echo base_url();?>";
</script>
<script>
    audiojs.events.ready(function() {
        var as = audiojs.createAll();
    });
</script>

