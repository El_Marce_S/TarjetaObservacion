<!-- page content -->
<div class="right_col" role="main">
	<!-- top tiles -->
	<div class="row tile_count">
		<div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
			<span class="count_top"> <i class="fa fa-user"></i> Tarjeta Totales</span>
			<div class="count green"> <?php echo $this->db->count_all('tarjetatotal'); ?> </div>

		</div>
		<div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
			<span class="count_top"><i class="fa fa-user"></i>Tarjetas Sin Transcripcion</span>
			<div
				class="count red"> <?php echo $this->db->where('EstadoTarjeta', 0)->from("tarjetatotal")->count_all_results() ?>
			</div>
		</div>

	</div>
	<!-- /top tiles -->
	<br/>
	<div class="row">


		<div class="col-md-4 col-sm-4 col-xs-12">
			<div class="x_panel tile fixed_height_320 overflow_hidden">
				<img src="<?php echo base_url() ?>Assets/img/total.png">

			</div>
		</div>


		<div class="col-md-4 col-sm-4 col-xs-12">
			<div class="x_panel tile fixed_height_320">
				<img src="<?php echo base_url() ?>Assets/img/empro_logo.png">

			</div>
		</div>

		<div class="col-md-4 col-sm-4 col-xs-12">
			<div id="container">

				<!-- Code generated at https://WeatherWidget.io -->
				<a class="weatherwidget-io" href="https://forecast7.com/es/n17d41n66d17/cochabamba/" data-label_1="Cochabamba" data-label_2="Pronostico del Tiempo" data-theme="white" >LONDON WEATHER</a>
				<script>
					!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="https://weatherwidget.io/js/widget.min.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","weatherwidget-io-js");
				</script>
				<!-- / Code generated at https://WeatherWidget.io -->

			</div>
		</div>





	</div>



</div>
</div>
</div>
</div>
<!-- /page content -->

<!-- footer content -->
<footer>
	<div class="pull-right">
		Desarrollado para Total <a href="htpp://www.emproltda.com"> Por Empro LTDA</a>
	</div>
	<div class="clearfix"></div>
</footer>
<!-- /footer content -->
</div>
</div>

<!-- jQuery -->
<script src="<?php echo base_url(); ?>Assets/Gentelella/vendors/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap -->
<script src="<?php echo base_url(); ?>Assets/Gentelella/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- FastClick -->
<script src="<?php echo base_url(); ?>Assets/Gentelella/vendors/fastclick/lib/fastclick.js"></script>
<!-- NProgress -->
<script src="<?php echo base_url(); ?>Assets/Gentelella/vendors/nprogress/nprogress.js"></script>
<!-- Chart.js -->
<script src="<?php echo base_url(); ?>Assets/Gentelella/vendors/Chart.js/dist/Chart.min.js"></script>
<!-- gauge.js -->
<script src="<?php echo base_url(); ?>Assets/Gentelella/vendors/gauge.js/dist/gauge.min.js"></script>
<!-- bootstrap-progressbar -->
<script
	src="<?php echo base_url(); ?>Assets/Gentelella/vendors/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
<!-- iCheck -->
<script src="<?php echo base_url(); ?>Assets/Gentelella/vendors/iCheck/icheck.min.js"></script>
<!-- Skycons -->
<script src="<?php echo base_url(); ?>Assets/Gentelella/vendors/skycons/skycons.js"></script>
<!-- Flot -->
<script src="<?php echo base_url(); ?>Assets/Gentelella/vendors/Flot/jquery.flot.js"></script>
<script src="<?php echo base_url(); ?>Assets/Gentelella/vendors/Flot/jquery.flot.pie.js"></script>
<script src="<?php echo base_url(); ?>Assets/Gentelella/vendors/Flot/jquery.flot.time.js"></script>
<script src="<?php echo base_url(); ?>Assets/Gentelella/vendors/Flot/jquery.flot.stack.js"></script>
<script src="<?php echo base_url(); ?>Assets/Gentelella/vendors/Flot/jquery.flot.resize.js"></script>
<!-- Flot plugins -->
<script src="<?php echo base_url(); ?>Assets/Gentelella/vendors/flot.orderbars/js/jquery.flot.orderBars.js"></script>
<script src="<?php echo base_url(); ?>Assets/Gentelella/vendors/flot-spline/js/jquery.flot.spline.min.js"></script>
<script src="<?php echo base_url(); ?>Assets/Gentelella/vendors/flot.curvedlines/curvedLines.js"></script>
<!-- DateJS -->
<script src="<?php echo base_url(); ?>Assets/Gentelella/vendors/DateJS/build/date.js"></script>
<!-- JQVMap -->
<script src="<?php echo base_url(); ?>Assets/Gentelella/vendors/jqvmap/dist/jquery.vmap.js"></script>
<script src="<?php echo base_url(); ?>Assets/Gentelella/vendors/jqvmap/dist/maps/jquery.vmap.world.js"></script>
<script src="<?php echo base_url(); ?>Assets/Gentelella/vendors/jqvmap/examples/js/jquery.vmap.sampledata.js"></script>
<!-- bootstrap-daterangepicker -->
<script src="../vendors/moment/min/moment.min.js"></script>
<script src="<?php echo base_url(); ?>Assets/Gentelella/vendors/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- Custom Theme Scripts -->
<script src="<?php echo base_url(); ?>Assets/Gentelella/build/js/custom.min.js"></script>

</body>
</html>
