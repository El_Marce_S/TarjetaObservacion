<!-- page content -->
<form action="<?php echo base_url() ?>index.php/Total/TotalController/SubirAudios" method="post"
      enctype="multipart/form-data">
    <div class="right_col" role="main">
        <div class="clearfix"></div>

        <div class="col-md-8 col-sm-8 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2><i class="fa fa-bars"></i> Datos del Audio
                        <small>Ingrese Todos los datos</small>
                    </h2>

                    <div class="clearfix"></div>
                </div>
                <div class="x_content">

                    <div class="col-xs-3">
                        <!-- required for floating -->
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs tabs-left">
                            <li class="active"><a href="#home" data-toggle="tab">Tarjeta Total</a>
                            </li>
                            <li><a href="#settings" data-toggle="tab">Finalizar</a>
                            </li>
                        </ul>
                    </div>

                    <div class="col-xs-9">
                        <!-- Tab panes -->
                        <div class="tab-content">
                            <div class="tab-pane active" id="home">
                                <p class="lead">Datos Del Audio</p>
                                <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                                    <input type="text" name="Topico"
                                           class="form-control has-feedback-left" id="inputSuccess2"
                                           placeholder="Topico">
                                    <span class="fa fa-user form-control-feedback left"
                                          aria-hidden="true"></span>
                                </div>

                                <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                                    <input type="text" name="Lugar" class="form-control" id="inputSuccess3"
                                           placeholder="Lugar">
                                    <span class="fa fa-user form-control-feedback right"
                                          aria-hidden="true"></span>
                                </div>

                                <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                                    <input type="date" name="Fecha"
                                           class="form-control has-feedback-left" id="inputSuccess2"
                                           placeholder="Fecha"><span
                                            class="fa fa-user form-control-feedback left"
                                            aria-hidden="true"></span>
                                </div>

                                <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                                    <input type="time" name="Hora" class="form-control has-feedback-right"
                                           id="inputSuccess4"
                                           placeholder="Hora">
                                    <span class="fa fa-newspaper-o form-control-feedback right"
                                          aria-hidden="true"></span>
                                </div>
                                <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                                    <input type="date" name="UploadDate"
                                           class="form-control has-feedback-left" id="inputSuccess2"
                                           placeholder="Fecha de Subida"><span
                                            class="fa fa-user form-control-feedback left"
                                            aria-hidden="true"></span>
                                </div>




                                <div class="form-group">
                                    <div class='col-sm-6'>
                                        Seleccione El Audio 1
                                        <div class="form-group">
                                            <div class="form-group">
                                                <input type="file" name="AudioDesc" class="form-control">
                                                Seleccione El Audio 2
                                            </div><div class="form-group">
                                                <input type="file" name="AudioAcc" class="form-control">
                                            </div>

                                        </div>

                                    </div>
                                </div>


                            </div>


                            <div class="tab-pane" id="settings">
                                <div class="ln_solid"></div>
                                <div class="form-group">
                                    <h1 class=""> Verifique antes de finalizar.</h1>
                                    <div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-3">
                                        <button type="button" class="btn btn-primary">Cancelar</button>
                                        <button class="btn btn-primary" value="Limpiar" type="reset">Limpiar
                                            Registros
                                        </button>
                                        <button type="submit" name="Guardar" value="Guardar"
                                                class="btn btn-success">Registrar
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>

        </div>
</form>
</div>
<!-- /page content -->

<!-- footer content -->
<footer>
    <div class="pull-right">
        Empro LTDA<a href="https://colorlib.com">Empro</a>
    </div>
    <div class="clearfix"></div>
</footer>
<!-- /footer content -->
</div>
</div>

<!-- jQuery -->
<script src="<?php echo base_url(); ?>Assets/Gentelella/vendors/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap -->
<script src="<?php echo base_url(); ?>Assets/Gentelella/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- FastClick -->
<script src="<?php echo base_url(); ?>Assets/Gentelella/vendors/fastclick/lib/fastclick.js"></script>
<!-- NProgress -->
<script src="<?php echo base_url(); ?>Assets/Gentelella/vendors/nprogress/nprogress.js"></script>
<!-- jQuery Smart Wizard -->
<script src="<?php echo base_url(); ?>Assets/Gentelella/vendors/jQuery-Smart-Wizard/js/jquery.smartWizard.js"></script>
<!-- Custom Theme Scripts -->
<script src="<?php echo base_url(); ?>Assets/Gentelella/build/js/custom.min.js"></script>


<!-- bootstrap-daterangepicker -->
<script src="<?php echo base_url(); ?>Assets/Gentelella/vendors/moment/min/moment.min.js"></script>
<script src="<?php echo base_url(); ?>Assets/Gentelella/vendors/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- bootstrap-datetimepicker -->
<script src="<?php echo base_url(); ?>Assets/Gentelella/vendors/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>
<!-- Initialize datetimepicker -->

<script>
    $('#myDatepicker').datetimepicker({
        format: 'YYYY-MM-DD'
    });

    $('#myDatepicker2').datetimepicker({
        format: 'YYYY-MM-DD'
    });

    $('#myDatepicker3').datetimepicker({
        format: 'YYYY-MM-DD'
    });

</script>

</body>
</html>