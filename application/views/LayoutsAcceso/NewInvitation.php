<!-- page content -->
<form action="<?php echo base_url() ?>index.php/SAdmin/SAdmin/Crear_Persona" method="post"
      enctype="multipart/form-data">
    <div class="right_col" role="main">
        <div class="clearfix"></div>

        <div class="col-md-8 col-sm-8 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2><i class="fa fa-bars"></i> Datos de Funcionario
                        <small>Ingrese Todos los datos</small>
                    </h2>

                    <div class="clearfix"></div>
                </div>
                <div class="x_content">

                    <div class="col-xs-3">
                        <!-- required for floating -->
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs tabs-left">
                            <li class="active"><a href="#home" data-toggle="tab">Datos personales</a>
                            </li>
                            <li><a href="#profile" data-toggle="tab">Datos de contacto</a>
                            </li>
                            <li><a href="#messages" data-toggle="tab">Datos laborales</a>
                            </li>
                            <li><a href="#settings" data-toggle="tab">Finalizar</a>
                            </li>
                        </ul>
                    </div>

                    <div class="col-xs-9">
                        <!-- Tab panes -->
                        <div class="tab-content">
                            <div class="tab-pane active" id="home">
                                <p class="lead">Datos Personales</p>
                                <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                                    <input type="text" name="NombreCompleto"
                                           class="form-control has-feedback-left" id="inputSuccess2"
                                           placeholder="Nombre Completo">
                                    <span class="fa fa-user form-control-feedback left"
                                          aria-hidden="true"></span>
                                </div>

                                <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                                    <input type="text" name="ApPat" class="form-control" id="inputSuccess3"
                                           placeholder="Apellido Paterno">
                                    <span class="fa fa-user form-control-feedback right"
                                          aria-hidden="true"></span>
                                </div>

                                <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                                    <input type="text" name="ApMat"
                                           class="form-control has-feedback-left" id="inputSuccess2"
                                           placeholder="Apellido Materno"><span
                                        class="fa fa-user form-control-feedback left"
                                        aria-hidden="true"></span>
                                </div>

                                <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                                    <input type="text" name="Cedula" class="form-control has-feedback-right"
                                           id="inputSuccess4"
                                           placeholder="Cedula de Identidad">
                                    <span class="fa fa-newspaper-o form-control-feedback right"
                                          aria-hidden="true"></span>
                                </div>


                                <div class="form-group">
                                    <div class='col-sm-6'>
                                        Estado Civil
                                        <div class="form-group">
                                            <select class="form-control" name="EstCivil">
                                                <option value="">Sleccione una opción</option>
                                                <option value="Casado">Casado</option>
                                                <option value="Soltero">Soltero</option>
                                                <option value="Divorciado">Divorciado</option>
                                                <option value="Conviviente">Conviviente</option>
                                                <option value="Viudo">Viudo</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class='col-sm-6'>
                                        Fecha de Nacimiento
                                        <div class="form-group">
                                            <div class='input-group date' id='myDatepicker2'>
                                                <input type='text' class="form-control" name="FechaNac"/>
                                                <span class="input-group-addon">
                                                            <span class="glyphicon glyphicon-calendar"></span>
                                                            </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                                    <input type="text" name="NumHijos"
                                           class="form-control has-feedback-left" id="inputSuccess2"
                                           placeholder="Numero de hijos"><span
                                        class="fa fa-group form-control-feedback left"
                                        aria-hidden="true"></span>
                                </div>
                                <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                                    <input type="text" name="NumCta"
                                           class="form-control has-feedback-left" id="inputSuccess2"
                                           placeholder="Cuenta Bancaria"><span
                                        class="fa fa-bank form-control-feedback right"
                                        aria-hidden="true"></span>
                                </div>

                                <div class="form-group">
                                    <div class='col-sm-6'>
                                        Seleccione una Imágen
                                        <div class="form-group">
                                            <div class="form-group">
                                                <input type="file" name="foto_empleado" class="form-control">

                                            </div>

                                        </div>
                                    </div>
                                </div>


                            </div>

                            <div class="tab-pane " id="profile">
                                <p class="lead">Datos de Contacto</p>
                                <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                                    <input type="text" name="Direc"
                                           class="form-control has-feedback-left" id="inputSuccess2"
                                           placeholder="Direccion Completa">
                                    <span class="fa fa-map-marker form-control-feedback left"
                                          aria-hidden="true"></span>
                                </div>

                                <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                                    <input type="text" name="Telf" class="form-control" id="inputSuccess3"
                                           placeholder="Teléfono/ Celular">
                                    <span class="fa fa-phone-square form-control-feedback right"
                                          aria-hidden="true"></span>
                                </div>

                                <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                                    <input type="text" name="email"
                                           class="form-control has-feedback-left" id="inputSuccess2"
                                           placeholder="Correo electronico personal"><span
                                        class="fa fa-at form-control-feedback left"
                                        aria-hidden="true"></span>
                                </div>

                                <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                                    <input type="text" name="CtcPrsn" class="form-control" id="inputSuccess3"
                                           placeholder="Persona de Contacto">
                                    <span class="fa fa-user form-control-feedback right"
                                          aria-hidden="true"></span>
                                </div>

                                <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                                    <input type="text" name="Tel_Contact"
                                           class="form-control has-feedback-left" id="inputSuccess2"
                                           placeholder="Teléfono Persona de contacto"><span
                                        class="fa fa-phone form-control-feedback left"
                                        aria-hidden="true"></span>
                                </div>


                            </div>


                            <div class="tab-pane" id="messages">
                                <p class="lead">Datos Laborales</p>

                                <div class='form-group'>
                                    <div class="col-sm-6">
                                        <div class='form-group'>
                                            <BR>
                                            <div class="form-group has-feedback">
                                                <input type="text" name="Prof"
                                                       class="form-control has-feedback-left" id="inputSuccess2"
                                                       placeholder=" Profesion">
                                                <span class="fa fa-map-marker form-control-feedback left"
                                                      aria-hidden="true"></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                                <div class="form-group">
                                    <div class='col-sm-6'>
                                        Fecha de Presentación
                                        <div class="form-group">
                                            <div class='input-group date' id='myDatepicker3'>
                                                <input type='text' class="form-control" name="FechaPre"/>
                                                <span class="input-group-addon">
                                                            <span class="glyphicon glyphicon-calendar"></span>
                                                            </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                                    <input type="number" name="NumItem"
                                           class="form-control has-feedback-left" id="inputSuccess2"
                                           placeholder="Numero de Item"><span
                                        class="fa fa-file-text-o form-control-feedback left"
                                        aria-hidden="true"></span>
                                </div>

                                <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                                    <input type="number" name="NumCont"
                                           class="form-control has-feedback-left" id="inputSuccess2"
                                           placeholder="Numero de Contrato"><span
                                        class="fa fa-file-text form-control-feedback left"
                                        aria-hidden="true"></span>
                                </div>
                            </div>
                            <div class="tab-pane" id="settings">
                                <div class="ln_solid"></div>
                                <div class="form-group">
                                    <h1 class=""> Verifique antes de finalizar.</h1>
                                    <div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-3">
                                        <button type="button" class="btn btn-primary">Cancelar</button>
                                        <button class="btn btn-primary" value="Limpiar" type="reset">Limpiar
                                            Registros
                                        </button>
                                        <button type="submit" name="Guardar" value="Guardar"
                                                class="btn btn-success">Registrar
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>

        </div>
</form>
</div>
<!-- /page content -->

<!-- footer content -->
<footer>
    <div class="pull-right">
        Gentelella - Bootstrap Admin Template by <a href="https://colorlib.com">Colorlib</a>
    </div>
    <div class="clearfix"></div>
</footer>
<!-- /footer content -->
</div>
</div>

<!-- jQuery -->
<script src="<?php echo base_url(); ?>Assets/Gentelella/vendors/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap -->
<script src="<?php echo base_url(); ?>Assets/Gentelella/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- FastClick -->
<script src="<?php echo base_url(); ?>Assets/Gentelella/vendors/fastclick/lib/fastclick.js"></script>
<!-- NProgress -->
<script src="<?php echo base_url(); ?>Assets/Gentelella/vendors/nprogress/nprogress.js"></script>
<!-- jQuery Smart Wizard -->
<script src="<?php echo base_url(); ?>Assets/Gentelella/vendors/jQuery-Smart-Wizard/js/jquery.smartWizard.js"></script>
<!-- Custom Theme Scripts -->
<script src="<?php echo base_url(); ?>Assets/Gentelella/build/js/custom.min.js"></script>


<!-- bootstrap-daterangepicker -->
<script src="<?php echo base_url(); ?>Assets/Gentelella/vendors/moment/min/moment.min.js"></script>
<script src="<?php echo base_url(); ?>Assets/Gentelella/vendors/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- bootstrap-datetimepicker -->
<script src="<?php echo base_url(); ?>Assets/Gentelella/vendors/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>
<!-- Initialize datetimepicker -->

<script>
    $('#myDatepicker').datetimepicker({
        format: 'YYYY-MM-DD'
    });

    $('#myDatepicker2').datetimepicker({
        format: 'YYYY-MM-DD'
    });

    $('#myDatepicker3').datetimepicker({
        format: 'YYYY-MM-DD'
    });

</script>

</body>
</html>