<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Usuarios_model extends CI_Model {

    function __construct()
    {
        parent::__construct();
    }
    
    public function LogIn($username,$password){
        $this->db->where("username",$username);
        $this->db->where("password",$password);
        /* obtiene los resultados de la comparativa y almacena en una variable "Resultados"*/
        $resultados = $this->db->get("usr_prsn");
        /* compara si los resultados son mas de una fila */
        if ($resultados-> num_rows () > 0){
            return $resultados->row();


        }
        else{
            return false;
        }
    }


}