<?php

class Crud_model extends CI_Model
{
    var $table = "personal";
    var $select_column = array(
        "idPersonal",
        "PerApa",
        "PerAma",
        "PerNom",
        "PerCI",
        "PerEst");
    var $order_column = array(
        null,
        "PerApa",
        "PerAma",
        "PerNom",
        "PerCI",
        "PerEst",
        null,
        null);

    function make_query()
    {
        $this->db->select($this->select_column);
        $this->db->from($this->table);
        if (isset($_POST["search"]["value"])) {
            $this->db->like("PerApa", $_POST["search"]["value"]);
            $this->db->or_like("PerAma", $_POST["search"]["value"]);
            $this->db->or_like("PerNom", $_POST["search"]["value"]);
        }
        if (isset($_POST["order"])) {
            $this->db->order_by($this->order_column[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else {
            $this->db->order_by('idPersonal', 'DESC');

        }
    }

    function make_dataTables()
    {
        $this->make_query();
        if($_POST['length'] != -1){
            $this->db->limit($_POST['length'], $_POST['start']);
        }
        $query = $this->db->get();
        return $query->result();
    }

    function get_filtered_data()
    {
        $this->make_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    function get_all_data()
    {
        $this->db->select('*');
        $this->db->from($this->table);
        return $this->db->count_all_results();
        $data = array();
        foreach ($fetch_data as $row) {
            $sub_array = array();
            $sub_array[] = $row->PerApa;
            $sub_array[] = $row->PerAma;
            $sub_array[] = $row->PerNom;
            $sub_array[] = $row->PerCI;
            $sub_array[] = '<button type="button" name="update" id="' . $row->id . '"class=btn btn-warning btn-xs">Actualizar</button>';
            $sub_array[] = '<button type="button" name="update" id="' . $row->id . '"class=btn btn-warning btn-xs">Eliminar</button>';
            $data[] = $sub_array;
        }
        $output = array(
            "draw" => intval($_POST["draw"]),
            "recordsTotal" => $this->Crud_model->get_all_data(),
            "recordsFiltered" => $this->Crud_model->get_filtered_data(),
            "data" => $data
        );
        echo json_encode($output);
    }

}

?>