<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';
require APPPATH . 'libraries/Uuid.php';

use Restserver\Libraries\REST_Controller;

class TotalApi extends REST_Controller
{
	function __construct()
	{
		// Construct the parent class
		parent::__construct();
		$this->load->helper('array');
		$this->load->helper('url');
		$this->load->library('xmlrpc');
		$this->load->library('xmlrpcs');


	}

	public function total_post()
	{
		echo '<h3>Informacion de Tarjeta</h3>';
//        $query=$this->db->query('select * from tarjetatotal');
//        echo json_encode($query->result());
		echo 'El reporte es sobre:' . $this->post('TopicoReporte');
		echo '<br>Audio de Descripcion:' . $this->post('AudioTarjeta');
//        echo '<br>Audio de Accion:' . $this->post('AudioAccion');
		echo '<br> Lugar:' . $this->post('Lugar');
		echo '<br> Fecha de Reporte:' . $this->post('Fecha');
		echo '<br> Hora de Reporte:' . $this->post('Hora');
		echo '<br> Fecha de Subida:' . $this->post('FechaDeSubida');
		echo '<br> Estado de Tarjeta:' . $this->post('EstadoTarjeta');
		echo '<br> Transcripcion:' . $this->post('Transcripcion');
	}

	public function FindAll_get()
	{
		echo json_encode($this->Cards_model->FindAll());
	}

	public function FindByID_get($IdTarjeta)
	{
		echo json_encode($this->Cards_model->FindID($IdTarjeta));
	}

	public function FindByToken_get($Token)
	{
		echo json_encode($this->Cards_model->FindToken($Token));
	}

	public function NewRecords_post()
	{
		$config['upload_path'] = './Audios/';
		$config['allowed_types'] = '*';
		$config['file_name'] = date('YmdHis') . 'audiotarjeta';

		$this->load->library('upload', $config);
		if (!$this->upload->do_upload('AudioDesc')) {
			$error = $this->upload->display_errors();
		} else {
			$data1 = $this->upload->data();
			$Audio1 = $data1['file_name'];

			date('Y-m-d H:i:s');
			$now = date('Y-m-d H:i:s', strtotime('-4 hours'));


			$NuevaTarjeta = array(
				'TopicoReporte' => $this->post('TopicoReporte'),
				'AudioDescripcion' => $Audio1,
//            'AudioAccion' =>$Audio2,
//				'Lugar' => $this->post('Lugar'),
				'Latitud' => $this->post('Latitud'),
				'Longitud' => $this->post('Longitud'),
				'Fecha' => $this->post('Fecha'),
				'Hora' => $this->post('Hora'),
				'ReglaDeOro' => $this->post('ReglaDeOro'),
				'FechaDeSubida' => $now,
				'EstadoTarjeta' => 0,
				'Token' => $this->post('Token'),
			);
			$this->session->set_userdata($NuevaTarjeta);


			$LatToApi = $this->session->userdata('Latitud');
			$LongToApi = $this->session->userdata('Longitud');
			$GoogleApiKey = 'AIzaSyAtKPaFJkqOsmHRV8dmzf3FMRYHbuVVLoE';

			$prox = $LatToApi . ',' . $LongToApi;
			$HereMapsApiUrl = 'https://maps.googleapis.com/maps/api/geocode/json?latlng=' . str_replace('%2C', ',', urlencode($prox)) . '&key=' . $GoogleApiKey;


			//Initialize cURL.
			$ch = curl_init();

			//Set the URL that you want to GET by using the CURLOPT_URL option.
			curl_setopt($ch, CURLOPT_URL, $HereMapsApiUrl);

			//Set CURLOPT_RETURNTRANSFER so that the content is returned as a variable.
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

			//Set CURLOPT_FOLLOWLOCATION to true to follow redirects.
			curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);

			//Execute the request.
			$data = curl_exec($ch);

			//Close the cURL handle.
			curl_close($ch);

			//Print the data out onto the page.
//			echo $data;
			$Direccion = json_decode($data, true);


//			$Location=($Direccion['results'][0]["address_components"][1]["long_name"]);
			$FromatedAddress=($Direccion['results'][0]["formatted_address"]);
//			print $Ciudad.','.$Location;
//			print $FromatedAddress;

			$LugarNuevaTarjeta = array(


				'Lugar' => $FromatedAddress

			);
			$resultado=array_merge($NuevaTarjeta,$LugarNuevaTarjeta);
			$this->Cards_model->insert($resultado);



                ////////////////////////////////////////////////////////////////
				////////////////////////////////////////////////////////////////
				// a partir de la linea de abajo  es codigo sin función///////
				///////////////////////////////////////////////////////////

//							$data = json_encode($json_data);
////FCM API end-point
//			$url = 'https://fcm.googleapis.com/fcm/send';
////api_key in Firebase Console -> Project Settings -> CLOUD MESSAGING -> Server key
//			$server_key = 'AAAACxS20cA:APA91bG22BLf_mWUDpjH4YRTnAXIi3YVwF0aNtAO-i7tf-RIep9VfdS-SQD7DxeOKQdsxrnWlwZVa8dJrwxJNk-lWZj-4gkHw03JusXWysqwMsU5RJNKBWCexrR-WhK6x-x1aszhlB7G';
////header with content_type api key
//			$headers = array(
//				'Content-Type:application/json',
//				'Authorization:key='.$server_key
//			);
////CURL request to route notification to FCM connection server (provided by Google)
//			$ch = curl_init();
//			curl_setopt($ch, CURLOPT_URL, $url);
//			curl_setopt($ch, CURLOPT_POST, true);
//			curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
//			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
//			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
//			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
//			curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
//			$result = curl_exec($ch);
//			if ($result === FALSE) {
//				die('Oops! FCM Send Error: ' . curl_error($ch));
//			}
//			curl_close($ch);




		}
	}
}
