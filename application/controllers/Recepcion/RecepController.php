<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class RecepController extends CI_Controller
{

    function index(){

    }

    public function __construct()
    {
        parent:: __construct();
        if (!$this->session->userdata("login")) {
            redirect('Auth');
        }
    }


    public function Home()
    {

        $this->load->view('LayoutsRecepcion/0Header');
        $this->load->view('LayoutsRecepcion/1SideMenu');
        $this->load->view('LayoutsRecepcion/2TopNavigation');
        $this->load->view('LayoutsRecepcion/Home');

    }


    public function Guia_Telefonica()
    {
        $query=$this->db->query('select * from ListinTelefonico');
        $rows=$query->result();
        $info=array('result'=>$rows);

        $this->load->view('LayoutsRecepcion/0Header');
        $this->load->view('LayoutsRecepcion/1SideMenu');
        $this->load->view('LayoutsRecepcion/2TopNavigation');
        $this->load->view('LayoutsRecepcion/GuiaTelefonica',$info);
    }

    public function Agregar_Contactos()
    {
        $UsrCr = $this->session->userdata('id');

        if (isset($_POST['Guardar'])) {


            $dataReg = array
            (
                'Nombre' => $_POST['NOMBRE'],
                'TelefonoFijo1' => $_POST['Tel1'],
                'TelefonoFijo2' => $_POST['Tel2'],
                'TelIP' => $_POST['TelIP'],
                'Celular' => $_POST['Cel'],
                'Observacion' => $_POST['Obs'],
                'Dato2' => $_POST['Ref'],
                'Usr_Creator' => $UsrCr


            );

            $this->db->insert('ListinTelefonico', $dataReg);
            $this->session->set_flashdata('DONE','Conctacto agregado correctamente');
            redirect('Recepcion/RecepController/Agregar_Contactos');
        }

        $this->load->view('LayoutsRecepcion/0Header');
        $this->load->view('LayoutsRecepcion/1SideMenu');
        $this->load->view('LayoutsRecepcion/2TopNavigation');
        $this->load->view('LayoutsRecepcion/AgregarContactos');
    }


}