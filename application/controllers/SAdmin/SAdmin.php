<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class SAdmin extends CI_Controller
{

    function index()
    {

    }

    public function __construct()
    {
        parent:: __construct();
        if (!$this->session->userdata("login")) {
            redirect('Auth');
        }
    }

    public function Home()
    {


        $this->load->view("LayoutsSAdmin/0Header");
        $this->load->view("LayoutsSAdmin/1SideMenu");
        $this->load->view("LayoutsSAdmin/2TopNavigation");
        $this->load->view("LayoutsSAdmin/Home");

    }

    public function Crear_Persona()
    {
        $config['upload_path'] = './Assets/ProfilePics/';
        $config['allowed_types'] = 'gif|jpg|png';

        $this->load->library('upload', $config);
        if (!$this->upload->do_upload('foto_empleado')) {
            $error = $this->upload->display_errors();

        } else {

            $data = $this->upload->data();
            $FotName = $data['file_name'];
        }


        $UsrCr = $this->session->userdata('id');

        if (isset($_POST['Guardar'])) {


            $dataPrsn = array
            (
                'PerApa' => $_POST['ApPat'],
                'PerAma' => $_POST['ApMat'],
                'PerNom' => $_POST['NombreCompleto'],
                'PerCI' => $_POST['Cedula'],
                'PerDir' => $_POST['Direc'],
                'PerTel' => $_POST['Telf'],
                'PerEmail' => $_POST['email'],
                'PerPeC' => $_POST['CtcPrsn'],
                'PerECi' => $_POST['EstCivil'],
                'PerFeN' => $_POST['FechaNac'],
                'PerCta' => $_POST['NumCta'],
                'NumHijos' => $_POST['NumHijos'],
                'PerEst' => 0,
                'PerFot' => $FotName,
                'Tel_Contact' => $_POST['Tel_Contact'],
                'Usr_Creator' => $UsrCr

            );
            $dataEmployee = array
            (
                'Profesión' => $_POST['Prof'],
                'LabFei' => $_POST['FechIngr'],
                'LabNuC' => $_POST['NumCont'],
                'LabNui' => $_POST['NumItem'],
            );

            $this->db->insert('Personal', $dataPrsn);
            $this->db->insert('DLaboral', $dataEmployee);
            $this->session->set_flashdata("success", "La Cuenta ha sido Registrada, el usuario puede ingresar al sistema");
            redirect('SAdmin/SAdmin/Home');
        }

        $this->load->view('LayoutsSAdmin/0Header');
        $this->load->view('LayoutsSAdmin/1SideMenu');
        $this->load->view('LayoutsSAdmin/2TopNavigation');
        $this->load->view('LayoutsSAdmin/Create_Prsn');


    }

    public function Modificar_Persona()

    {
        $query = $this->db->query('select * from Personal where PerEst=0');
        $rows = $query->result();
        $info = array('result' => $rows);

        $UsrCr = $this->session->userdata('id');

        if (isset($_POST['Actualizar'])) {


            $ModifiedPrsn = array
            (
                'PerApa' => $_POST['modal_PerApa'],
                'PerAma' => $_POST['modal_PerAma'],
                'PerNom' => $_POST['modal_PerNom'],
                'Usr_Creator' => $UsrCr
            );

            $this->db->where('idPersonal', $_POST['modal_idPersonal']);
            $this->db->update('Personal', $ModifiedPrsn);


            redirect('SAdmin/SAdmin/Modificar_Persona');
        }


        if (isset($_POST['Eliminar'])) {


            $DeletedPrsn = array
            (
                'PerApa' => '',
                'PerAma' => '',
                'PerNom' => '',
                'Usr_Creator' => $UsrCr
            );

            $this->db->where('idPersonal', $_POST['modal_idPersonal']);
            $this->db->delete('Personal', $DeletedPrsn);


            redirect('SAdmin/SAdmin/Modificar_Persona');
        }


        $this->load->view('LayoutsSAdmin/0Header');
        $this->load->view('LayoutsSAdmin/1SideMenu');
        $this->load->view('LayoutsSAdmin/2TopNavigation');
        $this->load->view('LayoutsSAdmin/Modify_Prsn', $info);
    }

    public function getPersonas (){
        $this->load->model('prsn_model');
       $resultado= $this->prsn_model->getPersonas();
        echo json_encode($resultado);
    }

    public function Crear_Usuario()
    {


        $query = $this->db->query('select * from Personal where PerEst=0');
        $rows = $query->result();
        $info = array('result' => $rows);


        $this->load->view('LayoutsSAdmin/0Header');
        $this->load->view('LayoutsSAdmin/1SideMenu');
        $this->load->view('LayoutsSAdmin/2TopNavigation');
        $this->load->view('LayoutsSAdmin/Create_Usr', $info);
    }

    public function Guia_Telefonica()
    {
        $query = $this->db->query('select * from ListinTelefonico');
        $rows = $query->result();
        $info = array('result' => $rows);

        $this->load->view('LayoutsSAdmin/0Header');
        $this->load->view('LayoutsSAdmin/1SideMenu');
        $this->load->view('LayoutsSAdmin/2TopNavigation');
        $this->load->view('LayoutsSAdmin/GuiaTelefonica', $info);
    }

    public function Modificar_Usuario()

    {
        $query = $this->db->query('select * from Usuarios');
        $rows = $query->result();
        $usr = array('result' => $rows);


        $this->load->view('LayoutsSAdmin/0Header');
        $this->load->view('LayoutsSAdmin/1SideMenu');
        $this->load->view('LayoutsSAdmin/2TopNavigation');
        $this->load->view('LayoutsSAdmin/Modify_Usr', $usr);
    }
}