<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class TotalController extends CI_Controller
{

    public function __construct()
    {
        parent:: __construct();

    }
    function index(){
        $this->load->view('LayoutsTotal/0Header');
        $this->load->view('LayoutsTotal/1SideMenu');
        $this->load->view('LayoutsTotal/2TopNavigation');
        $this->load->view('LayoutsTotal/Home');
    }

    public function Home()
    {
        $this->load->view('LayoutsTotal/0Header');
        $this->load->view('LayoutsTotal/1SideMenu');
        $this->load->view('LayoutsTotal/2TopNavigation');
        $this->load->view('LayoutsTotal/Home');
    }

    public function SubirAudios()
    {
        $config['upload_path'] = './Assets/Audios/';
        $config['allowed_types'] = 'mp3|3gp|wav';

        $this->load->library('upload', $config);
        if (!$this->upload->do_upload('AudioDesc')) {
            $error = $this->upload->display_errors();

        } else {

            $data = $this->upload->data();
            $Audio1 = $data['file_name'];
        }
        if (!$this->upload->do_upload('AudioAcc')) {
            $error = $this->upload->display_errors();

        } else {

            $data = $this->upload->data();
            $Audio2 = $data['file_name'];
        }

//        La siguiente linea hay q habilitarla si deseamos controlar quien sube que
//        $UsrCr = $this->session->userdata('id');

        if (isset($_POST['Guardar'])) {


            $dataReg = array
            (
                'TopicoReporte' => $_POST['Topico'],
                'AudioDescripcion' => $Audio1,
                'AudioAccion' => $Audio2,
                'Lugar' => $_POST['Lugar'],
                'Fecha' => $_POST['Fecha'],
                'Hora' => $_POST['Hora'],
                'FechaDeSubida' => $_POST['UploadDate'],
                'EstadoTarjeta' => 0,

            );

            $this->db->insert('tarjetatotal', $dataReg);
            $this->session->set_flashdata('DONE','Audios Guardados Correctamente');
            redirect('Total/TotalController/Home');
        }

        $this->load->view('LayoutsTotal/0Header');
        $this->load->view('LayoutsTotal/1SideMenu');
        $this->load->view('LayoutsTotal/2TopNavigation');
        $this->load->view('LayoutsTotal/TarjetaNuevoAudio');
    }

    public function TarjetasNuevas()

    {
        $query = $this->db->query('select * from tarjetatotal where EstadoTarjeta=0');
        $rows = $query->result();
        $info = array('result' => $rows);
        $UsrCr = $this->session->userdata('id');
        if (isset($_POST['Actualizar'])) {

            $TrancscripcionAudio = array
            (
                'Transcripcion' => $_POST['modal_Transcripcion'],
                'EstadoTarjeta' => 1
            );

            $this->db->where('IdTarjeta', $_POST['modal_IdTarjeta']);
            $this->db->update('tarjetatotal', $TrancscripcionAudio);

            redirect('Total/TotalController/TarjetasNuevas');
        }

        if (isset($_POST['Eliminar'])) {

            $DeletedPrsn = array
            (
                'PerApa' => '',
                'PerAma' => '',
                'PerNom' => '',
                'Usr_Creator' => $UsrCr
            );

            $this->db->where('IdTarjeta', $_POST['modal_idPersonal']);
            $this->db->delete('tarjetatotal', $DeletedPrsn);


            redirect('Total/TotalController/TarjetasNuevas');
        }


        $this->load->view('LayoutsTotal/0Header');
        $this->load->view('LayoutsTotal/1SideMenu');
        $this->load->view('LayoutsTotal/2TopNavigation');
        $this->load->view('LayoutsTotal/TarjetasAudios', $info);
    }

    public function getAudios (){
        $this->load->model('Audios_model');
        $resultado= $this->Audios_model->getAudios();
        echo json_encode($resultado);
    }

	public function ReportesTranscritos() {
		$query = $this->db->query('select * from tarjetatotal WHERE EstadoTarjeta = 1');
		$rows = $query->result();
		$info = array('result' => $rows);

		$this->load->view('LayoutsTotal/0Header');
		$this->load->view('LayoutsTotal/1SideMenu');
		$this->load->view('LayoutsTotal/2TopNavigation');
		$this->load->view('LayoutsTotal/ReporteTranscrito', $info);
	}




}
