$('#document').ready(function () {
    $('#tabla_audios').html('<thead>\n' +
        '                    <tr>\n' +
        '                        <th>Topico</th>\n' +
        '                        <th>Nombre de Archivo de Audio</th>\n' +
        '                        <th>Lugar</th>\n' +
        '                        <th>Fecha de Reporte</th>\n' +
        '                        <th>Hora de Reporte</th>\n' +
        '                        <th>Regla de Oro</th>\n' +
        '                        <th>Fecha de Subida</th>\n' +
        '                        <th>Transcripción Pendiente </th>\n' +
        '                    </tr>\n' +
        '                    </thead>');

    $.post(baseurl + "index.php/Total/TotalController/getAudios",
        function (data) {
            var audio = JSON.parse(data);
            $.each(audio, function (i, row) {
                $('#tabla_audios').append(
                    '<tr>' +
                    '<td>' + row.TopicoReporte + '</td>' +
                    '<td>' + row.AudioDescripcion + '</td>' +
                    '<td>' +row.Lugar+'</td> '+
                    '<td>' + row.Fecha+ '</td>' +
                    '<td>' + row.Hora+ '</td>' +
                    '<td>' + row.ReglaDeOro+ '</td>' +
                    '<td>' + row.FechaDeSubida+ '</td>' +
                    '<td align="center"><a href="#" ' +
                    'class=" btn btn-round btn-success boton" ' +
                    'data-toggle="modal"  data-target="#ModalAgregarTranscripcion" ' +
					'onClick="selAudio(\''+row.IdTarjeta+'\',\''+row.AudioDescripcion+'\');">Agregar Transcripción</a></td>'+
					'</tr>'
                );
              });
        });
});

//con esta funcion pasamos los paremtros a los text del modal.

// selAudio = function (IdTarjeta,AudioDescripcion) {
//
// 	$('#modal_IdTarjeta').val(IdTarjeta);
// 	$('#modal_AudioDescripcion').val(AudioDescripcion);
// 	//     // $('#modal_Transcripcion').val(Transcripcion);
//
//
// };
//

selAudio = function (IdTarjeta,AudioDescripcion) {
	var prefix =  '../../../Audios/';
	$('#modal_IdTarjeta').val(IdTarjeta);
	$('#modal_AudioDescripcion').val(AudioDescripcion);
	//     // $('#modal_Transcripcion').val(Transcripcion);

	var $player = $('#modal_AudioDescripcion');
	var $player_src = $('#modal_AudioDescripcion_src');
	$player_src.attr('src',prefix+AudioDescripcion);
	$player[0].pause();
	$player[0].load();
	$player.oncanplaythrough =$player[0].play();
	alert(AudioDescripcion);
};
